import sys

print("Welcome to BlackJack! First, you have to know that the game is configured in this way:\n"
      "- 6 decks\n"
      "- No Peek (the dealer can have BlackJack and you may not know it)\n"
      "- No surrender \n"
      "- It is possible to double after split\n"
      "- The dealer stands with soft 17\n"
      "Estimated casino edge for these rules: 0.55%\n"
      "Write 'play' anc click enter to start")

possible_cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]



def check_user_cards(user_input):
    if len(user_input) == 2:
        for i in [0, 1]:
            user_input[i] = int(user_input[i])
            if user_input[i] not in possible_cards:
                print("Error, invalid input, the numbers are not between 1 and 10")
                return 0
            else:
                if i == 2:
                    return 1
    else:
        print("Error, invalid input, introduce two numbers between 1 and 10 split by a whitespace")
        return 0


def play():
    try:
        print(
            "Introduce your two cards split by a white space, 10, J, Q and K have to be a 10, the ACES have to be a 1")
        user_cards = input().split()
        check = check_user_cards(user_cards)
        if check == 0:
            exit()
        print("Introduce dealer's card,  10, J, Q and K have to be a 10, the ACES have to be a 1")
        dealer_card = int(input())
        cards_sum = user_cards[0] + user_cards[1]
        if dealer_card not in possible_cards:
            print("Invalid dealer card")
            exit()
        # For pairs
        if user_cards[0] == user_cards[1]:
            if cards_sum == 2:
                if dealer_card == 1:
                    print("Hit")
                else:
                    print("Split")
            elif cards_sum == 20:
                print("Stand")
            elif cards_sum == 18:
                if dealer_card in [1, 7, 10]:
                    print("Stand")
                else:
                    print("Split")
            elif cards_sum == 16:
                if dealer_card in [1, 10]:
                    print("Hit")
                else:
                    print("Split")
            elif cards_sum in [4, 6]:
                if dealer_card in [1,8,9,10]:
                    print("Hit")
                else:
                    print("Split")
            elif cards_sum == 8:
                if dealer_card in [5,6]:
                    print("Split")
                else:
                    print("Hit")
            elif cards_sum == 10:
                if dealer_card in [1,10]:
                    print("Hit")
                else:
                    print("Double down")
            else:
                if dealer_card in [2,3,4,5,6]:
                    print("Split")
                elif dealer_card in [1,8,9,10]:
                    print("Hit")
                else:
                    if cards_sum == 12:
                        print("Hit")
                    else:
                        print("Split")
        # For soft hands
        elif user_cards[0] == 1 or user_cards[1] == 1:
            if cards_sum in [9,10]:
                print("Stand")
            elif cards_sum == 8:
                if dealer_card in [3,4,5,6]:
                    print("Double down or stand if not allowed")
                elif dealer_card in [2,7,8]:
                    print("Stand")
            elif cards_sum in [5,6] and dealer_card in [4,5,6]:
                print("Double down")
            elif cards_sum == 7 and dealer_card in [3,4,5,6]:
                print("Double down")
            elif cards_sum in [3,4] and dealer_card in [5,6]:
                print("Double down")
            else:
                print("Hit")
        # For hard hands
        else:
            if cards_sum <= 8:
                print("Hit")
            elif cards_sum >= 17:
                print("Stand")
            elif cards_sum in [13, 14, 15, 16]:
                if dealer_card == 1 or dealer_card >= 7:
                    print("Hit")
                else:
                    print("Stand")
            elif cards_sum in [10, 11]:
                if dealer_card in [1, 10]:
                    print("Hit")
                else:
                    print("Double down")
            elif cards_sum == 9:
                if dealer_card in [3, 4, 5, 6]:
                    print("Double down")
                else:
                    print("Hit")
            else:
                if dealer_card in [4, 5, 6]:
                    print("Stand")
                else:
                    print("Hit")
        play()
    except:
        print("General error, exit")
        exit()


first_input = input()
if first_input == "play":
    play()
else:
    print("Error, the program has finished")
    exit()
